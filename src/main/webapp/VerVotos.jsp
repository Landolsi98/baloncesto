<table>
    <thead>
      <tr>
        <th>Player Name</th>
        <th>Number of Votes</th>
      </tr>
    </thead>
    <tbody>
      <% 
        modelo.abrirConexion();
        try {
          Statement stmt = modelo.con.createStatement();
          ResultSet rs = stmt.executeQuery("SELECT * FROM Jugadores");
          while (rs.next()) {
            String nombre = rs.getString("Nombre");
            int votos = rs.getInt("votos");
            out.println("<tr><td>" + nombre + "</td><td>" + votos + "</td></tr>");
          }
          rs.close();
          stmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
        modelo.cerrarConexion();
      %>
    </tbody>
  </table>
  

